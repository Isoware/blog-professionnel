<?php

namespace Isoware;

use PDO;

class BDConnexion
{
    protected static $dtb;

    /**
     * @return PDO
     */
    private static function dbConnect(): PDO
    {
        $dtb = new PDO('mysql:host=localhost;dbname=global_db;charset=utf8', 'root', '');
        $dtb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dtb->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        return $dtb;
    }

    /**
     * @return PDO
     */
    public static function getDb(): PDO
    {
        if (isset(self::$dtb) || self::$dtb === null) {
            self::$dtb = self::dbConnect();
        }
        return self::$dtb;
    }
}
