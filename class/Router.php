<?php

namespace Isoware;

use Isoware\Controller\AdminComments;
use Isoware\Controller\AdminPosts;
use Isoware\Controller\AdminUsers;
use Isoware\Controller\Blog;
use Isoware\Controller\Home;
use Isoware\Controller\NotFound;
use Isoware\DTO\UtilisateurDTO;
use Isoware\Exception\CaptchaError;
use Isoware\Exception\DroitsException;
use Isoware\Exception\ElementInexistantException;
use Isoware\Exception\EmailAlreadyUsed;
use Isoware\Exception\RequeteException;
use Isoware\Exception\SaisieIncorrecte;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

require_once $_SESSION['root'] . 'class/TwigExtend.php';

class Router
{
    /**
     * @var string $page
     */
    private $page;
    /**
     * @var string  $action
     */
    private $action;
    /**
     * @var string[] $callablePages
     */
    private $callablePages;
    /**
     * @var string[] $adminPages
     */
    private $adminPages;
    /**
     * @var array $postVariables
     */
    private $postVariables;

    /**
     * Router constructor.
     * @param null $page
     * @param null $action
     */
    public function __construct($page = null, $action = null)
    {
        $this->page = $page ?? 'home';
        $this->action = $action;

        $this->callablePages = [
            'home',
            'blog',
            'notFound'
        ];

        $this->adminPages = [
            'adminUsers',
            'adminPosts',
            'adminComments'
        ];

        if (
            (!in_array($this->page, $this->adminPages)
            || !(new UtilisateurDTO())->hasRight('administrateur'))
            && !in_array($this->page, $this->callablePages)
        ) {
                $this->page = 'notFound';
        }

        $_SESSION['current_page'] = $this->page;

        $this->postVariables = $this->cleanPost();

        try {
            $this->appelController();
        } catch (
            DroitsException
            | ElementInexistantException
            | EmailAlreadyUsed
            | CaptchaError
            | SaisieIncorrecte
            | RequeteException $e
        ) {
            echo $e;
        } catch (\Exception $e) {
            new NotFound();
        }
    }

    /**
     * @throws CaptchaError
     * @throws DroitsException
     * @throws ElementInexistantException
     * @throws EmailAlreadyUsed
     * @throws Exception\SaisieIncorrecte
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function appelController()
    {
        switch ($this->page) {
            case 'home':
                new Home($this->action, $this->postVariables);
                break;
            case 'blog':
                new Blog($this->action, $this->postVariables);
                break;
            case 'adminUsers':
                new AdminUsers($this->action, $this->postVariables);
                break;
            case 'adminPosts':
                new AdminPosts($this->action, $this->postVariables);
                break;
            case 'adminComments':
                new AdminComments($this->action, $this->postVariables);
                break;
            case 'notFound':
            default:
                new NotFound($this->action, $this->postVariables);
        }
    }

    /**
     * @return array
     */
    private function cleanPost(): array
    {
        $post = $_POST;
        foreach ($post as $key => $value) {
            $post[$key] = mb_convert_encoding($value, 'UTF-8', 'UTF-8');
        } return $post;
    }
}
