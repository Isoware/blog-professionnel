<?php

namespace Isoware\Exception;

use Throwable;

class SaisieIncorrecte extends \Exception
{
    /**
     * SaisieIncorrecte constructor.
     * @param null $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = null, $code = 403, Throwable $previous = null)
    {
        if (!isset($message)) {
            $message = 'Un des champs saisis est incorrecte.';
        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return "{$this->message}\n";
    }
}
