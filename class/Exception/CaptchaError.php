<?php

namespace Isoware\Exception;

use Throwable;

class CaptchaError extends \Exception
{
    /**
     * CaptchaError constructor.
     * @param null $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = null, $code = 498, Throwable $previous = null)
    {
        if (!isset($message)) {
            $message = 'Erreur.';
        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return "{$this->message}\n";
    }
}
