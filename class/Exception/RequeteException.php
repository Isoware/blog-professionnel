<?php

namespace Isoware\Exception;

use Throwable;

class RequeteException extends \Exception
{
    /**
     * RequeteException constructor.
     * @param null $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = null, $code = 400, Throwable $previous = null)
    {
        if (!isset($message)) {
            $message = 'Erreur lors de la requête.';
        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return "[{$this->code}]: {$this->message}\n";
    }
}
