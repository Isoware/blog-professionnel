<?php

namespace Isoware\Exception;

use Throwable;

class DroitsException extends \Exception
{
    /**
     * DroitsException constructor.
     * @param null $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = null, $code = 403, Throwable $previous = null)
    {
        if (!isset($message)) {
            $message = 'Vous n\'avez pas les droits pour cette action.';
        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return "[{$this->code}]: {$this->message}\n";
    }
}
