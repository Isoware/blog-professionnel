<?php

namespace Isoware\Exception;

use Throwable;

class ElementInexistantException extends \Exception
{
    /**
     * ElementInexistantException constructor.
     * @param null $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = null, $code = 404, Throwable $previous = null)
    {
        if (!isset($message)) {
            $message = 'Impossible de trouver cet élément.';
        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return "[{$this->code}]: {$this->message}\n";
    }
}
