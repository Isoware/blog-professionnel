<?php

namespace Isoware\Exception;

use Throwable;

class EmailAlreadyUsed extends \Exception
{
    /**
     * EmailAlreadyUsed constructor.
     * @param null $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = null, $code = 403, Throwable $previous = null)
    {
        if (!isset($message)) {
            $message = 'Email déjà utilisée.';
        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return "{$this->message}\n";
    }
}
