<?php

namespace Isoware;

class ReCaptcha
{
    /**
     * @return bool
     */
    public function testCaptcha(): bool
    {
        if (empty($_POST['recaptchaResponse'])) {
            return false;
        }
        // On prépare l'URL
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=6LdLhoIaAAAAAGmxryM1J35XEQBfY2VCJaTxmnV-&response={$_POST['recaptchaResponse']}";

        // On vérifie si curl est installé
        if (function_exists('curl_version')) {
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($curl);
        } else {
            // On utilisera file_get_contents
            $response = file_get_contents($url);
        }

        // On vérifie qu'on a une réponse
        if (empty($response)) {
            return false;
        }

        $data = json_decode($response);
        if (!$data->success) {
            return false;
        }

        return true;
    }
}
