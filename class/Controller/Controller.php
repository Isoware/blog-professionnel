<?php

namespace Isoware\Controller;

use App\Twig\TwigExtend;
use Isoware\DTO\UtilisateurDTO;
use Isoware\Exception\CaptchaError;
use Isoware\Exception\ElementInexistantException;
use Isoware\Exception\EmailAlreadyUsed;
use Isoware\Exception\RequeteException;
use Isoware\Exception\SaisieIncorrecte;
use Isoware\Model\Utilisateur;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;

class Controller
{
    /**
     * @var string $page
     */
    protected $page;

    /**
     * @var string $action
     */
    protected $action;

    /**
     * @var Environment $twig
     */
    protected static $twig;

    /**
     * @var array $postVariables
     */
    protected $postVariables;

    /**
     * Controller constructor.
     * @param $page
     * @param array $postVariables
     */
    public function __construct($page, $postVariables = [])
    {
        $this->page = $page;
        $this->postVariables = $postVariables;
        if (!isset($this->twig)) {
            $this->setTwig();
        }

        self::$twig->addGlobal('current_page', $this->page);
    }

    /**
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function display(): string
    {
        return self::$twig->render($this->page . '.twig', $this->getDataUtilisateur());
    }

    private function setTwig()
    {
        $loader = new FilesystemLoader(__DIR__ . '/../../templates');

        self::$twig = new Environment($loader, [
            'cache' => false, // __DIR__ . '/tmp'
        ]);

        self::$twig->addExtension(new TwigExtend());
    }

    /**
     * @param array $data
     * @return array
     */
    protected function getDataUtilisateur($data = []): array
    {
        return array_merge($data, [
            'utilisateur' => unserialize($_SESSION['utilisateur'] ?? '')
        ]);
    }

    /**
     * @throws CaptchaError
     * @throws EmailAlreadyUsed
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    protected function handleAction()
    {
        $this->handleGlobalAction();
    }

    /**
     * @return bool
     * @throws CaptchaError
     * @throws ElementInexistantException
     * @throws EmailAlreadyUsed
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    protected function handleGlobalAction(): bool
    {
        switch ($this->action) {
            case 'sign':
                $this->sign();
                break;
            case 'connect':
                $this->connect();
                break;
            case 'disconnect':
                $this->disconnect();
                break;
            case 'affiche':
            default:
                echo $this->display();
        }
        return true;
    }

    /**
     * @throws CaptchaError
     * @throws EmailAlreadyUsed
     * @throws RequeteException
     * @throws SaisieIncorrecte
     */
    private function sign()
    {
        $utilisateur = new Utilisateur();

        if (
            !isset($this->postVariables['nom'])
            || !isset($this->postVariables['prenom'])
            || !isset($this->postVariables['pseudo'])
            || !isset($this->postVariables['email'])
            || !isset($this->postVariables['password'])
        ) {
            throw new SaisieIncorrecte();
        }

        $utilisateur->setNom($this->postVariables['nom'] ?? '');
        $utilisateur->setPrenom($this->postVariables['prenom'] ?? '');
        $utilisateur->setPseudo($this->postVariables['pseudo'] ?? '');
        $utilisateur->setMail($this->postVariables['email'] ?? '');
        $utilisateur->setPassword($this->postVariables['password'] ?? '');

        (new UtilisateurDTO())->sign($utilisateur);
    }

    /**
     * @throws RequeteException
     * @throws SaisieIncorrecte
     * @throws ElementInexistantException
     */
    private function connect()
    {
        $utilisateur = new Utilisateur();
        $utilisateur->setMail($this->postVariables['email'] ?? '');
        $utilisateur->setPassword($this->postVariables['password'] ?? '');

        (new UtilisateurDTO())->connect($utilisateur);
    }

    public function disconnect()
    {
        session_destroy();
    }
}
