<?php

namespace Isoware\Controller;

use Isoware\Exception\CaptchaError;
use Isoware\Exception\EmailAlreadyUsed;
use Isoware\Exception\RequeteException;
use Isoware\Exception\SaisieIncorrecte;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class NotFound extends Controller
{
    /**
     * NotFound constructor.
     * @param string $action
     * @param array $postVariables
     * @throws CaptchaError
     * @throws EmailAlreadyUsed
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    public function __construct($action = 'affiche', $postVariables = [])
    {
        parent::__construct('notFound', $postVariables);

        $this->action = $action;

        $this->handleAction();
    }
}
