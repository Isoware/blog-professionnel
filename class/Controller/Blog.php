<?php

namespace Isoware\Controller;

use Isoware\DTO\CommentaireDTO;
use Isoware\DTO\PostDTO;
use Isoware\DTO\UtilisateurDTO;
use Isoware\Exception\CaptchaError;
use Isoware\Exception\DroitsException;
use Isoware\Exception\ElementInexistantException;
use Isoware\Exception\EmailAlreadyUsed;
use Isoware\Exception\RequeteException;
use Isoware\Exception\SaisieIncorrecte;
use Isoware\Model\Commentaire;
use Isoware\Model\Post;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Blog extends Controller
{
    /**
     * Blog constructor.
     * @param string $action
     * @param array $postVariables
     * @throws CaptchaError
     * @throws DroitsException
     * @throws ElementInexistantException
     * @throws EmailAlreadyUsed
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    public function __construct($action = 'affiche', $postVariables = [])
    {
        parent::__construct('blog', $postVariables);
        $this->action = $action;

        $this->handleAction();
    }

    /**
     * @return string
     * @throws ElementInexistantException
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    public function display(): string
    {
        $data['posts'] = (new postDTO())->getAllValidPosts();
        $data['utilisateurs'] = (new UtilisateurDTO())->getAllUtilisateurs();
        $data = $this->getDataUtilisateur($data);
        return self::$twig->render($this->page . '.twig', $data);
    }

    /**
     * @throws CaptchaError
     * @throws DroitsException
     * @throws ElementInexistantException
     * @throws EmailAlreadyUsed
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    protected function handleAction()
    {
        switch ($this->action) {
            case 'createPost':
                $this->createPost();
                break;
            case 'deletePost':
                $this->deletePost();
                break;
            case 'modifyPost':
                $this->modifyPost();
                break;
            case 'addComment':
                $this->addComment();
                break;
            case 'deleteComment':
                $this->deleteComment();
                break;
            default:
                parent::handleAction();
        }
    }

    /**
     * @throws RequeteException
     * @throws SaisieIncorrecte
     */
    private function createPost()
    {
        $post = new Post();

        if (
            !isset($this->postVariables['titre'])
            || !isset($this->postVariables['chapo'])
            || !isset($this->postVariables['contenu'])
        ) {
            throw new SaisieIncorrecte();
        }

        $post->setTitre($this->postVariables['titre']);
        $post->setChapo($this->postVariables['chapo']);
        $post->setContenu($this->postVariables['contenu']);

        (new PostDTO())->createPost($post);
    }

    /**
     * @return bool
     * @throws DroitsException
     * @throws ElementInexistantException
     * @throws RequeteException
     * @throws SaisieIncorrecte
     */
    private function deletePost(): bool
    {
        if (!(new UtilisateurDTO())->hasRight('modifier_post')) {
            throw new DroitsException();
        }

        if (!isset($this->postVariables['id'])) {
            throw new SaisieIncorrecte();
        }

        $post = new Post();
        $post->setId($this->postVariables['id']);

        $postDTO = new PostDTO();
        if (!$postDTO->existPost($post)) {
            throw new ElementInexistantException();
        }
        return $postDTO->deletePost($post);
    }

    /**
     * @return bool
     * @throws DroitsException
     * @throws RequeteException
     * @throws SaisieIncorrecte
     */
    private function modifyPost(): bool
    {
        if (!(new UtilisateurDTO())->hasRight('modifier_post')) {
            throw new DroitsException();
        }

        if (
            !isset($this->postVariables['id'])
            || !isset($this->postVariables['titre'])
            || !isset($this->postVariables['chapo'])
            || !isset($this->postVariables['contenu'])
            || !isset($this->postVariables['auteur'])
        ) {
            throw new SaisieIncorrecte();
        }

        $post = new Post();
        $post->setId($this->postVariables['id']);
        $post->setTitre($this->postVariables['titre']);
        $post->setChapo($this->postVariables['chapo']);
        $post->setContenu($this->postVariables['contenu']);
        $post->setIdAuteur($this->postVariables['auteur']);

        return (new PostDTO())->modifyPost($post);
    }

    /**
     * @return bool
     * @throws ElementInexistantException
     * @throws RequeteException
     * @throws SaisieIncorrecte
     */
    public function addComment(): bool
    {
        $post = new Post();

        if (!isset($this->postVariables['idPost'])) {
            throw new SaisieIncorrecte();
        }

        $post->setId($this->postVariables['idPost']);

        if (!(new PostDTO())->existPost($post)) {
            throw new ElementInexistantException();
        }

        $commentaire = new Commentaire();
        $commentaire->setContenu($this->postVariables['contenu']);

        return (new CommentaireDTO())->addComment($post, $commentaire);
    }

    /**
     * @return bool
     * @throws DroitsException
     * @throws ElementInexistantException
     * @throws RequeteException
     * @throws SaisieIncorrecte
     */
    private function deleteComment(): bool
    {
        if (!(new UtilisateurDTO())->hasRight('modifier_post')) {
            throw new DroitsException();
        }

        if (!isset($this->postVariables['idComment'])) {
            throw new SaisieIncorrecte();
        }

        $comment = new Commentaire();
        $comment->setId($this->postVariables['idComment']);

        $commentaireDTO = new CommentaireDTO();
        if (!$commentaireDTO->existComment($comment)) {
            throw new ElementInexistantException();
        }
        return $commentaireDTO->deleteComment($comment);
    }
}
