<?php

namespace Isoware\Controller;

use Isoware\DTO\CommentaireDTO;
use Isoware\Exception\CaptchaError;
use Isoware\Exception\EmailAlreadyUsed;
use Isoware\Exception\SaisieIncorrecte;
use Isoware\Model\Commentaire;
use Isoware\DTO\UtilisateurDTO;
use Isoware\Exception\DroitsException;
use Isoware\Exception\ElementInexistantException;
use Isoware\Exception\RequeteException;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class AdminComments extends Controller
{
    /**
     * AdminComments constructor.
     * @param string $action
     * @param array $postVariables
     * @throws CaptchaError
     * @throws DroitsException
     * @throws ElementInexistantException
     * @throws EmailAlreadyUsed
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    public function __construct($action = 'affiche', $postVariables = [])
    {
        parent::__construct('adminComments', $postVariables);
        $this->action = $action;

        $this->handleAction();
    }

    /**
     * @return string
     * @throws ElementInexistantException
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    public function display(): string
    {
        $data['comments'] = (new CommentaireDTO())->getAllInvalidComments();
        $data['utilisateurs'] = (new UtilisateurDTO())->getAllUtilisateurs();
        $data = $this->getDataUtilisateur($data);
        return self::$twig->render($this->page . '.twig', $data);
    }

    /**
     * @throws CaptchaError
     * @throws DroitsException
     * @throws ElementInexistantException
     * @throws EmailAlreadyUsed
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    protected function handleAction()
    {
        switch ($this->action) {
            case 'validComment':
                $this->validComment();
                break;
            case 'deleteComment':
                $this->deleteComment();
                break;
            default:
                parent::handleAction();
        }
    }

    /**
     * @return bool
     * @throws DroitsException
     * @throws ElementInexistantException
     * @throws RequeteException
     */
    public function validComment(): bool
    {
        if (!(new UtilisateurDTO())->hasRight('administrateur')) {
            throw new DroitsException();
        }

        $comment = new Commentaire();
        $comment->setId($this->postVariables['idComment']);

        $commentaireDTO = new CommentaireDTO();
        if (!$commentaireDTO->existComment($comment)) {
            throw new ElementInexistantException();
        }
        return $commentaireDTO->validComment($comment);
    }

    /**
     * @return bool
     * @throws DroitsException
     * @throws ElementInexistantException
     * @throws RequeteException
     */
    private function deleteComment(): bool
    {
        if (!(new UtilisateurDTO())->hasRight('administrateur')) {
            throw new DroitsException();
        }

        $comment = new Commentaire();
        $comment->setId($this->postVariables['idComment']);

        $commentaireDTO = new CommentaireDTO();
        if (!$commentaireDTO->existComment($comment)) {
            throw new ElementInexistantException();
        }
        return $commentaireDTO->deleteComment($comment);
    }
}
