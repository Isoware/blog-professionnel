<?php

namespace Isoware\Controller;

use Isoware\DTO\PostDTO;
use Isoware\DTO\UtilisateurDTO;
use Isoware\Exception\CaptchaError;
use Isoware\Exception\DroitsException;
use Isoware\Exception\ElementInexistantException;
use Isoware\Exception\EmailAlreadyUsed;
use Isoware\Exception\RequeteException;
use Isoware\Exception\SaisieIncorrecte;
use Isoware\Model\Post;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class AdminPosts extends Controller
{
    /**
     * AdminPosts constructor.
     * @param string $action
     * @param array $postVariables
     * @throws CaptchaError
     * @throws DroitsException
     * @throws ElementInexistantException
     * @throws EmailAlreadyUsed
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    public function __construct($action = 'affiche', $postVariables = [])
    {
        parent::__construct('adminPosts', $postVariables);
        $this->action = $action;

        $this->handleAction();
    }

    /**
     * @return string
     * @throws ElementInexistantException
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    public function display(): string
    {
        $data['posts'] = (new PostDTO())->getAllInvalidPosts();
        $data['utilisateurs'] = (new UtilisateurDTO())->getAllUtilisateurs();
        $data = $this->getDataUtilisateur($data);
        return self::$twig->render($this->page . '.twig', $data);
    }

    /**
     * @throws CaptchaError
     * @throws DroitsException
     * @throws ElementInexistantException
     * @throws EmailAlreadyUsed
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    protected function handleAction()
    {
        switch ($this->action) {
            case 'validPost':
                $this->validPost();
                break;
            case 'suppressPost':
                $this->suppressPost();
                break;
            default:
                parent::handleAction();
        }
    }

    /**
     * @return bool
     * @throws DroitsException
     * @throws ElementInexistantException
     * @throws RequeteException
     */
    public function validPost(): bool
    {
        if (!(new UtilisateurDTO())->hasRight('administrateur')) {
            throw new DroitsException();
        }

        $post = new Post();
        $post->setId($this->postVariables['idPost']);

        $postDTO = new PostDTO();
        if (!$postDTO->existPost($post)) {
            throw new ElementInexistantException();
        }
        return $postDTO->validPost($post);
    }

    /**
     * @return bool
     * @throws DroitsException
     * @throws ElementInexistantException
     * @throws RequeteException
     */
    private function suppressPost(): bool
    {
        if (!(new UtilisateurDTO())->hasRight('administrateur')) {
            throw new DroitsException();
        }

        $post = new Post();
        $post->setId($this->postVariables['idPost']);

        $postDTO = new PostDTO();
        if (!$postDTO->existPost($post)) {
            throw new ElementInexistantException();
        }
        return $postDTO->deletePost($post);
    }
}
