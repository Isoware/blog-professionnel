<?php

namespace Isoware\Controller;

use Isoware\Contact;
use Isoware\Exception\CaptchaError;
use Isoware\Exception\EmailAlreadyUsed;
use Isoware\Exception\RequeteException;
use Isoware\Exception\SaisieIncorrecte;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Home extends Controller
{

    /**
     * Home constructor.
     * @param string $action
     * @param array $postVariables
     * @throws CaptchaError
     * @throws EmailAlreadyUsed
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    public function __construct($action = 'affiche', $postVariables = [])
    {
        parent::__construct('home', $postVariables);
        $this->action = $action;

        $this->handleAction();
    }

    /**
     * @throws CaptchaError
     * @throws EmailAlreadyUsed
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws SaisieIncorrecte
     */
    protected function handleAction()
    {
        switch ($this->action) {
            case 'contact':
                $this->contact($this->postVariables);
                break;
            default:
                parent::handleAction();
        }
    }

    /**
     * @param $postVariables
     * @throws CaptchaError
     * @throws RequeteException
     * @throws SaisieIncorrecte
     */
    protected function contact($postVariables)
    {
        (new Contact())->sendMail($postVariables);
    }
}
