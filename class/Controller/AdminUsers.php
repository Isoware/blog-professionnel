<?php

namespace Isoware\Controller;

use Isoware\DTO\RoleDTO;
use Isoware\DTO\UtilisateurDTO;
use Isoware\Exception\CaptchaError;
use Isoware\Exception\DroitsException;
use Isoware\Exception\ElementInexistantException;
use Isoware\Exception\EmailAlreadyUsed;
use Isoware\Exception\RequeteException;
use Isoware\Exception\SaisieIncorrecte;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class AdminUsers extends Controller
{
    /**
     * AdminUsers constructor.
     * @param string $action
     * @param array $postVariables
     * @throws CaptchaError
     * @throws DroitsException
     * @throws ElementInexistantException
     * @throws EmailAlreadyUsed
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    public function __construct($action = 'affiche', $postVariables = [])
    {
        parent::__construct('adminUsers', $postVariables);
        $this->action = $action;

        $this->handleAction();
    }

    /**
     * @return string
     * @throws ElementInexistantException
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    public function display(): string
    {
        $data['roles'] = (new RoleDTO())->getAll();
        $data['utilisateurs'] = (new UtilisateurDTO())->getAllUtilisateurs();
        $data = $this->getDataUtilisateur($data);
        return self::$twig->render($this->page . '.twig', $data);
    }

    /**
     * @throws CaptchaError
     * @throws DroitsException
     * @throws ElementInexistantException
     * @throws EmailAlreadyUsed
     * @throws LoaderError
     * @throws RequeteException
     * @throws RuntimeError
     * @throws SaisieIncorrecte
     * @throws SyntaxError
     */
    protected function handleAction()
    {
        switch ($this->action) {
            case 'modifyUserRole':
                $this->modifyUserRole();
                break;
            case 'suppressUser':
                $this->suppressUser();
                break;
            default:
                parent::handleAction();
        }
    }

    /**
     * @return bool
     * @throws DroitsException
     * @throws RequeteException
     * @throws SaisieIncorrecte
     * @throws ElementInexistantException
     */
    private function modifyUserRole(): bool
    {
        if (!(new UtilisateurDTO())->hasRight('administrateur')) {
            throw new DroitsException();
        }

        if (!isset($this->postVariables['idRole']) || !isset($this->postVariables['idUtilisateur'])) {
            throw new SaisieIncorrecte();
        }

        $user = (new UtilisateurDTO())->getById($this->postVariables['idUtilisateur']);

        $role = (new RoleDTO())->getById($this->postVariables['idRole']);

        if (
            !(new UtilisateurDTO())->hasRight('administrateur_global')
            && ($role->getNom() === unserialize($_SESSION['utilisateur'])->getRole()->getNom()
            || $role->getNom() === 'admin_principal')
        ) {
            throw new DroitsException();
        }

        if ($user->getId() === unserialize($_SESSION['utilisateur'])->getId()) {
            throw new DroitsException();
        }

        return (new UtilisateurDTO())->modifyUserRole($user, $role);
    }

    /**
     * @return bool
     * @throws DroitsException
     * @throws RequeteException
     * @throws SaisieIncorrecte
     * @throws ElementInexistantException
     */
    private function suppressUser(): bool
    {
        if (!(new UtilisateurDTO())->hasRight('administrateur')) {
            throw new DroitsException();
        }

        if (!isset($this->postVariables['idUtilisateur'])) {
            throw new SaisieIncorrecte();
        }

        $user = (new UtilisateurDTO())->getById($this->postVariables['idUtilisateur']);

        if (
            !(new UtilisateurDTO())->hasRight('administrateur_global')
            && ($user->getRole()->getNom() === 'admin_principal'
            || $user->getRole()->getNom() === 'admin')
        ) {
            throw new DroitsException();
        }

        if ($user->getId() === unserialize($_SESSION['utilisateur'])->getId()) {
            throw new DroitsException();
        }

        return (new UtilisateurDTO())->suppressUser($user);
    }
}
