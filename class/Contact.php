<?php

namespace Isoware;

use Isoware\Exception\CaptchaError;
use Isoware\Exception\RequeteException;
use Isoware\Exception\SaisieIncorrecte;

class Contact
{
    /**
     * @param $postVariables
     * @throws CaptchaError
     * @throws RequeteException
     * @throws SaisieIncorrecte
     */
    public function sendMail($postVariables)
    {
        // Check for empty fields
        if (
            empty($postVariables['name'])
            || empty($postVariables['email'])
            || empty($postVariables['phone'])
            || empty($postVariables['message'])
            || !filter_var($postVariables['email'], FILTER_VALIDATE_EMAIL)
        ) {
            throw new SaisieIncorrecte();
        }

        if (!(new ReCaptcha())->testCaptcha()) {
            throw new CaptchaError();
        }

        $name = strip_tags(htmlspecialchars($postVariables['name']));
        $email = strip_tags(htmlspecialchars($postVariables['email']));
        $phone = strip_tags(htmlspecialchars($postVariables['phone']));
        $message = strip_tags(htmlspecialchars($postVariables['message']));

        // Create the email and send the message
        $mailto = "charly.printanier@hotmail.fr";
        $subject = "Contact depuis blog professionnel:  $name";
        $body = "You have received a new message from your website contact form.\n\n" . "Here are the details:\n\nName: $name\n\nEmail: $email\n\nPhone: $phone\n\nMessage:\n$message";
        $header = "From: noreply@charly-printanier.ovh\n";
        $header .= "Reply-To: $email";

        if (!mail($mailto, $subject, $body, $header)) {
            throw new RequeteException();
        }

        echo 'Valid';
    }
}
