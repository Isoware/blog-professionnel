<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigExtend extends AbstractExtension
{
    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('currentPage', [$this, 'currentPage'], ['needs_context' => true]),
        ];
    }

    /**
     * @param array $context
     * @param $page
     * @return string
     */
    public function currentPage(array $context, $page): string
    {
        return (isset($context['current_page']) && $page == $context['current_page']) ? 'active' : '';
    }
}
