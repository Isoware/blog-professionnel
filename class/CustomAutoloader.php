<?php

namespace Isoware;

class CustomAutoloader
{
    /**
     * @param $class_name
     */
    public static function autoload($class_name)
    {
        $class_name = str_replace('Isoware\\', '', $class_name);
        $class_name = str_replace('\\', '/', $class_name);

        require $_SESSION['root'] . 'class/' . $class_name . '.php';
    }

    public static function register()
    {
        spl_autoload_register([__CLASS__, 'autoload']);
    }
}
