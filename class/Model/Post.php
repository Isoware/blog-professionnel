<?php

namespace Isoware\Model;

use Isoware\Exception\SaisieIncorrecte;

class Post
{
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var string $titre
     */
    private $titre;
    /**
     * @var string $chapo
     */
    private $chapo;
    /**
     * @var string $contenu
     */
    private $contenu;
    /**
     * @var array $utilisateur
     * @see Utilisateur
     */
    private $utilisateur;
    /**
     * @var int $idAuteur
     */
    private $idAuteur;
    /**
     * @var string $datePublication
     */
    private $datePublication;
    /**
     * @var string $dateMAJ
     */
    private $dateMaj;
    /**
     * @var array $commentaires
     * @see Commentaire
     */
    private $commentaires;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Post
     */
    public function setId(int $id): Post
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitre(): string
    {
        return $this->titre;
    }

    /**
     * @param string $titre
     * @return $this
     * @throws SaisieIncorrecte
     */
    public function setTitre(string $titre): Post
    {
        if (strlen($titre) < 20 || strlen($titre) > 60) {
            throw new SaisieIncorrecte();
        }
        $this->titre = $titre;
        return $this;
    }

    /**
     * @return string
     */
    public function getChapo(): string
    {
        return $this->chapo;
    }

    /**
     * @param string $chapo
     * @return $this
     * @throws SaisieIncorrecte
     */
    public function setChapo(string $chapo): Post
    {
        if (strlen($chapo) < 50) {
            throw new SaisieIncorrecte();
        }
        $this->chapo = $chapo;
        return $this;
    }

    /**
     * @return string
     */
    public function getContenu(): string
    {
        return $this->contenu;
    }

    /**
     * @param string $contenu
     * @return $this
     * @throws SaisieIncorrecte
     */
    public function setContenu(string $contenu): Post
    {
        if (strlen($contenu) < 100) {
            throw new SaisieIncorrecte();
        }
        $this->contenu = $contenu;
        return $this;
    }

    /**
     * @return Utilisateur
     */
    public function getUtilisateur(): Utilisateur
    {
        return $this->utilisateur;
    }

    /**
     * @param Utilisateur $utilisateur
     * @return Post
     */
    public function setUtilisateur(Utilisateur $utilisateur): Post
    {
        $this->utilisateur = $utilisateur;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdAuteur(): int
    {
        return $this->idAuteur;
    }

    /**
     * @param int $idAuteur
     * @return Post
     */
    public function setIdAuteur(int $idAuteur): Post
    {
        $this->idAuteur = $idAuteur;
        return $this;
    }

    /**
     * @return string
     */
    public function getDatePublication(): string
    {
        return $this->datePublication;
    }

    /**
     * @param string $datePublication
     * @return Post
     */
    public function setDatePublication(string $datePublication): Post
    {
        $this->datePublication = $datePublication;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateMaj(): string
    {
        return $this->dateMaj ?? '';
    }

    /**
     * @param string $dateMaj
     * @return Post
     */
    public function setDateMaj(string $dateMaj): Post
    {
        $this->dateMaj = $dateMaj;
        return $this;
    }

    /**
     * @return array
     */
    public function getCommentaires(): array
    {
        return $this->commentaires;
    }

    /**
     * @param array $commentaires
     * @return Post
     */
    public function setCommentaires(array $commentaires): Post
    {
        $this->commentaires = $commentaires;
        return $this;
    }
}
