<?php

namespace Isoware\Model;

class Role
{
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var string $nom
     */
    private $nom;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Role
     */
    public function setId(int $id): Role
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     * @return Role
     */
    public function setNom(string $nom): Role
    {
        $this->nom = $nom;
        return $this;
    }
}
