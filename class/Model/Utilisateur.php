<?php

namespace Isoware\Model;

use Isoware\Exception\SaisieIncorrecte;

class Utilisateur
{
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var string $nom
     */
    private $nom;
    /**
     * @var string $prenom
     */
    private $prenom;
    /**
     * @var string $pseudo
     */
    private $pseudo;
    /**
     * @var string $mail
     */
    private $mail;
    /**
     * @var array $droits
     */
    private $droits;

    /**
     * @var string $password
     */
    private $password;

    /**
     * @var Role $role
     */
    private $role;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Utilisateur
     */
    public function setId(?int $id): Utilisateur
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param string|null $nom
     * @return $this
     * @throws SaisieIncorrecte
     */
    public function setNom(?string $nom): Utilisateur
    {
        if (strlen($nom) > 50) {
            throw new SaisieIncorrecte();
        }
        $this->nom = $nom;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     * @return $this
     * @throws SaisieIncorrecte
     */
    public function setPrenom(string $prenom): Utilisateur
    {
        if (strlen($prenom) > 50) {
            throw new SaisieIncorrecte();
        }
        $this->prenom = $prenom;
        return $this;
    }

    /**
     * @return string
     */
    public function getPseudo(): string
    {
        return $this->pseudo;
    }

    /**
     * @param string $pseudo
     * @return Utilisateur
     */
    public function setPseudo(string $pseudo): Utilisateur
    {
        if (strlen($pseudo) <= 20) {
            $this->pseudo = $pseudo;
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getMail(): string
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     * @return $this
     * @throws SaisieIncorrecte
     */
    public function setMail(string $mail): Utilisateur
    {
        if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            throw new SaisieIncorrecte();
        }
        $this->mail = $mail;
        return $this;
    }

    /**
     * @return array
     */
    public function getDroits(): array
    {
        return $this->droits;
    }

    /**
     * @param array $droits
     * @return Utilisateur
     */
    public function setDroits(array $droits): Utilisateur
    {
        $this->droits = $droits;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     * @throws SaisieIncorrecte
     */
    public function setPassword(string $password): Utilisateur
    {
        if (
            strlen($password) < 8
            || strlen($password > 20)
            || !preg_match('@[A-Z]@', $password)
            || !preg_match('@[a-z]@', $password)
            || !preg_match('@[0-9]@', $password)
        ) {
            throw new SaisieIncorrecte();
        }
        $this->password = $password;
        return $this;
    }

    /**
     * @return Utilisateur
     */
    public function unsetPassword(): Utilisateur
    {
        unset($this->password);
        return $this;
    }

    /**
     * @return Role
     */
    public function getRole(): Role
    {
        return $this->role;
    }

    /**
     * @param Role $role
     * @return Utilisateur
     */
    public function setRole(Role $role): Utilisateur
    {
        $this->role = $role;
        return $this;
    }
}
