<?php

namespace Isoware\Model;

use Isoware\Exception\SaisieIncorrecte;

class Commentaire
{
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var Post $post
     */
    private $post;
    /**
     * @var int $statut
     */
    private $statut;
    /**
     * @var string $contenu
     */
    private $contenu;
    /**
     * @var string $date
     */
    private $datePublication;
    /**
     * @var Utilisateur $auteur
     * @see Utilisateur
     */
    private $auteur;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Commentaire
     */
    public function setId(int $id): Commentaire
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Post
     */
    public function getPost(): Post
    {
        return $this->post;
    }

    /**
     * @param Post $post
     * @return Commentaire
     */
    public function setPost(Post $post): Commentaire
    {
        $this->post = $post;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatut(): int
    {
        return $this->statut;
    }

    /**
     * @param int $statut
     * @return Commentaire
     */
    public function setStatut(int $statut): Commentaire
    {
        $this->statut = $statut;
        return $this;
    }

    /**
     * @return string
     */
    public function getContenu(): string
    {
        return $this->contenu;
    }

    /**
     * @param string $contenu
     * @return Commentaire
     * @throws SaisieIncorrecte
     */
    public function setContenu(string $contenu): Commentaire
    {
        if (strlen($contenu) < 20) {
            throw new SaisieIncorrecte();
        }
        $this->contenu = $contenu;
        return $this;
    }

    /**
     * @return string
     */
    public function getDatePublication(): string
    {
        return $this->datePublication;
    }

    /**
     * @param string $datePublication
     * @return Commentaire
     */
    public function setDatePublication(string $datePublication): Commentaire
    {
        $this->datePublication = $datePublication;
        return $this;
    }

    /**
     * @return Utilisateur
     */
    public function getAuteur(): Utilisateur
    {
        return $this->auteur;
    }

    /**
     * @param Utilisateur $auteur
     * @return Commentaire
     */
    public function setAuteur(Utilisateur $auteur): Commentaire
    {
        $this->auteur = $auteur;
        return $this;
    }
}
