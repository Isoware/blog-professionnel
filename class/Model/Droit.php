<?php

namespace Isoware\Model;

class Droit
{
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var string $nom
     */
    private $nom;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Droit
     */
    public function setId(int $id): Droit
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     * @return Droit
     */
    public function setNom(string $nom): Droit
    {
        $this->nom = $nom;
        return $this;
    }
}
