<?php

namespace Isoware\DTO;

use Isoware\BDConnexion;
use Isoware\Exception\CaptchaError;
use Isoware\Exception\ElementInexistantException;
use Isoware\Exception\EmailAlreadyUsed;
use Isoware\Exception\RequeteException;
use Isoware\Exception\SaisieIncorrecte;
use Isoware\Model\Role;
use Isoware\Model\Utilisateur;
use Isoware\ReCaptcha;

class UtilisateurDTO
{
    /**
     * @param Utilisateur $utilisateur
     * @return bool
     * @throws ElementInexistantException
     * @throws RequeteException
     * @throws SaisieIncorrecte
     */
    public function connect(Utilisateur $utilisateur): bool
    {
        $dtb = BDConnexion::getDb();

        $query = $dtb->prepare('
            SELECT id_utilisateur, nom, prenom, pseudo, id_role
            FROM utilisateur
            WHERE mail = :mail and mot_de_passe = :mdp;
        ');
        $query->execute([':mail' => $utilisateur->getMail(), ':mdp' => hash('sha256', $utilisateur->getPassword())]);

        if ($result = $query->fetch()) {
                        $utilisateur = new Utilisateur();
            $utilisateur->setId($result->id_utilisateur);
            $utilisateur->setNom($result->nom);
            $utilisateur->setPrenom($result->prenom);
            $utilisateur->setPseudo($result->pseudo);
            $utilisateur->setRole((new RoleDTO())->getById($result->id_role));
            $utilisateur->setDroits((new DroitDTO())->getDroitsByRole($utilisateur->getRole()));
            $utilisateur->unsetPassword();

            $_SESSION['utilisateur'] = serialize($utilisateur);

            echo 'Valid';
            return true;
        }
        return false;
    }

    /**
     * @param Utilisateur $utilisateur
     * @return bool
     * @throws CaptchaError
     * @throws EmailAlreadyUsed
     * @throws RequeteException
     */
    public function sign(Utilisateur $utilisateur): bool
    {
        $dtb = BDConnexion::getDb();

        if ($this->emailExist($utilisateur)) {
            throw new EmailAlreadyUsed();
        }

        if (!(new ReCaptcha())->testCaptcha()) {
            throw new CaptchaError();
        }

        $query = $dtb->prepare('
            INSERT INTO utilisateur (nom, prenom, pseudo, mail, mot_de_passe, id_role)
            VALUES (:nom, :prenom, :pseudo, :mail, :mdp, 3);
        ');
        $queryExecution = $query->execute([
            ':nom' => $utilisateur->getNom(),
            ':prenom' => $utilisateur->getPrenom(),
            ':pseudo' => $utilisateur->getPseudo(),
            ':mail' => $utilisateur->getMail(),
            ':mdp' => hash('sha256', $utilisateur->getPassword())
        ]);

        if (!$queryExecution) {
            throw new RequeteException();
        }

        echo 'Valid';
        return true;
    }

    /**
     * @param Utilisateur $utilisateur
     * @return bool
     */
    public function emailExist(Utilisateur $utilisateur): bool
    {
        $dtb = BDConnexion::getDb();

        $query = $dtb->prepare('
            SELECT id_utilisateur
            FROM utilisateur
            WHERE mail = :mail ;
        ');
        $query->execute([':mail' => $utilisateur->getMail()]);

        if ($query->fetch()) {
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @return Utilisateur
     * @throws SaisieIncorrecte
     */
    public function getById($id): Utilisateur
    {
        $utilisateur = null;

        $dtb = BDConnexion::getDb();

        $query = $dtb->prepare('
            SELECT id_utilisateur, nom, prenom, pseudo
            FROM utilisateur
            WHERE id_utilisateur = :id;
        ');
        $query->execute([':id' => $id]);

        if ($result = $query->fetch()) {
            $utilisateur =  new Utilisateur();
            $utilisateur->setId($result->id_utilisateur);
            $utilisateur->setNom($result->nom);
            $utilisateur->setPrenom($result->prenom);
            $utilisateur->setPseudo($result->pseudo);
        }

        return $utilisateur;
    }

    /**
     * @return array
     * @throws RequeteException
     * @throws SaisieIncorrecte
     * @throws ElementInexistantException
     */
    public function getAllUtilisateurs(): array
    {
        $dtb = BDConnexion::getDb();

        $query = $dtb->prepare('
            SELECT id_utilisateur, nom, prenom, pseudo, mail, id_role
            FROM utilisateur
        ');
        $query->execute();

        $utilisateurs = [];
        while ($result = $query->fetch()) {
            $utilisateur = new Utilisateur();
            $utilisateur->setId($result->id_utilisateur);
            $utilisateur->setNom($result->nom);
            $utilisateur->setPrenom($result->prenom);
            $utilisateur->setPseudo($result->pseudo);
            $utilisateur->setMail($result->mail);
            $utilisateur->setRole((new RoleDTO())->getById($result->id_role));

            $utilisateurs[] = $utilisateur;
        }

        return $utilisateurs;
    }

    /**
     * @param Utilisateur $user
     * @param Role $role
     * @return bool
     * @throws RequeteException
     */
    public function modifyUserRole(Utilisateur $user, Role $role): bool
    {
        $dtb = BDConnexion::getDb();

        $query = $dtb->prepare('
            UPDATE utilisateur
            SET id_role = :id_role
            WHERE id_utilisateur = :id_utilisateur
        ');

        $queryExecution = $query->execute([
            ':id_role' => $role->getId(),
            ':id_utilisateur' => $user->getId(),
        ]);

        if (!$queryExecution) {
            throw new RequeteException();
        }

        echo 'Valid';
        return true;
    }

    /**
     * @param Utilisateur $user
     * @return bool
     * @throws RequeteException
     */
    public function suppressUser(Utilisateur $user): bool
    {
        $dtb = BDConnexion::getDb();

        $query = $dtb->prepare('
            DELETE FROM utilisateur
            WHERE id_utilisateur = :id_utilisateur
        ');

        $queryExecution = $query->execute([
            ':id_utilisateur' => $user->getId(),
        ]);

        if (!$queryExecution) {
            throw new RequeteException();
        }

        echo 'Valid';
        return true;
    }

    public function hasRight(string $right): bool
    {
        $valide = false;

        if (isset($_SESSION['utilisateur'])) {
            foreach (unserialize($_SESSION['utilisateur'])->getDroits() as $droit) {
                if ($droit->getNom() === $right || $droit->getNom() === 'administrateur') {
                    $valide = true;
                }
            }
        }

        return $valide;
    }
}
