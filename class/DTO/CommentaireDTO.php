<?php

namespace Isoware\DTO;

use Isoware\BDConnexion;
use Isoware\Exception\RequeteException;
use Isoware\Exception\SaisieIncorrecte;
use Isoware\Model\Commentaire;
use Isoware\Model\Post;

class CommentaireDTO
{
    private const ID_STATUT_VALID = 1;
    private const ID_STATUT_INVALID = 2;

    /**
     * @param Post $post
     * @param Commentaire $commentaire
     * @return bool
     * @throws RequeteException
     */
    public function addComment(Post $post, Commentaire $commentaire): bool
    {
        $dtb = BDConnexion::getDB();
        $query = $dtb->prepare('
            INSERT INTO commentaire (id_post, id_utilisateur, contenu, id_statut)
            VALUES (:id_post, :id_utilisateur, :contenu, :statut)
        ');

        $queryExecution = $query->execute(
            [
                ':id_post' => $post->getId(),
                ':id_utilisateur' => unserialize($_SESSION['utilisateur'])->getId(),
                ':contenu' => $commentaire->getContenu(),
                ':statut' => self::ID_STATUT_INVALID
            ]
        );

        if (!$queryExecution) {
            throw new RequeteException();
        }

        echo 'Valid';
        return true;
    }

    /**
     * @param Post $post
     * @return array
     * @throws RequeteException
     * @throws SaisieIncorrecte
     */
    public function getPostCommentaires(Post $post): array
    {
        $dtb = BDConnexion::getDB();
        $query = $dtb->prepare('
            SELECT id_utilisateur, id_commentaire, contenu, date_publication
            FROM commentaire
            WHERE id_post = :id_post
            AND id_statut = :statut
        ');

        if (!$query->execute([':id_post' => $post->getId(), ':statut' => self::ID_STATUT_VALID])) {
            throw new RequeteException();
        }

        $commentaires = [];
        while ($result = $query->fetch()) {
            $commentaire = new Commentaire();
            $commentaire->setId($result->id_commentaire);
            $commentaire->setContenu($result->contenu);
            $commentaire->setDatePublication($result->date_publication);
            $commentaire->setAuteur((new UtilisateurDTO())->getById($result->id_utilisateur));
            $commentaires[] = $commentaire;
        }

        return $commentaires;
    }

    /**
     * @param Post $post
     * @return bool
     * @throws RequeteException
     */
    public function deletePostCommentaires(Post $post): bool
    {
        $dtb = BDConnexion::getDB();
        $query = $dtb->prepare('
            DELETE FROM commentaire
            WHERE id_post = :id_post
        ');

        if (!$query->execute([':id_post' => $post->getId()])) {
            throw new RequeteException();
        }

        return true;
    }

    /**
     * @param Commentaire $commentaire
     * @return bool
     * @throws RequeteException
     */
    public function deleteComment(Commentaire $commentaire): bool
    {
        $dtb = BDConnexion::getDB();
        $query = $dtb->prepare('
            DELETE FROM commentaire
            WHERE id_commentaire = :id_commentaire
        ');

        if (!$query->execute([':id_commentaire' => $commentaire->getId()])) {
            throw new RequeteException();
        }

        echo 'Valid';
        return true;
    }

    /**
     * @param Commentaire $commentaire
     * @return bool
     * @throws RequeteException
     */
    public function existComment(Commentaire $commentaire): bool
    {
        $dtb = BDConnexion::getDB();
        $query = $dtb->prepare('
            SELECT id_commentaire
            FROM commentaire
            WHERE id_commentaire = :id
        ');
        $query->execute([':id' => $commentaire->getId()]);

        if (!$query->fetch()) {
            throw new RequeteException();
        }

        return true;
    }

    /**
     * @return array
     * @throws RequeteException
     * @throws SaisieIncorrecte
     */
    public function getAllInvalidComments(): array
    {
        $commentaires = [];
        $dtb = BDConnexion::getDb();
        $query = $dtb->prepare('
            SELECT id_commentaire, id_utilisateur, id_post, contenu, date_publication
            FROM commentaire
            WHERE id_statut = :statut
            ORDER BY date_publication ASC;
        ');

        if (!$query->execute([':statut' => self::ID_STATUT_INVALID])) {
            throw new RequeteException();
        }

        while ($commentaire = $query->fetch()) {
            $temp_commentaire = new Commentaire();
            $temp_commentaire->setId($commentaire->id_commentaire);
            $temp_commentaire->setAuteur((new UtilisateurDTO())->getById($commentaire->id_utilisateur));
            $temp_commentaire->setPost((new PostDTO())->getById($commentaire->id_post));
            $temp_commentaire->setContenu($commentaire->contenu);
            $temp_commentaire->setDatePublication($commentaire->date_publication);
            $commentaires[] = $temp_commentaire;
        }

        return $commentaires;
    }

    /**
     * @param Commentaire $comment
     * @return bool
     * @throws RequeteException
     */
    public function validComment(Commentaire $comment): bool
    {
        $dtb = BDConnexion::getDb();
        $query = $dtb->prepare('
            UPDATE commentaire
            SET id_statut = :statut
            WHERE id_commentaire = :id
        ');

        if (!$query->execute([':id' => $comment->getId(), ':statut' => self::ID_STATUT_VALID])) {
            throw new RequeteException();
        }

        echo 'Valid';
        return true;
    }
}
