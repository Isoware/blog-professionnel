<?php

namespace Isoware\DTO;

use Isoware\BDConnexion;
use Isoware\Exception\RequeteException;
use Isoware\Exception\SaisieIncorrecte;
use Isoware\Model\Post;

class PostDTO
{
    private const ID_STATUT_VALID = 1;
    private const ID_STATUT_INVALID = 2;

    /**
     * @param Post $post
     * @return bool
     * @throws RequeteException
     */
    public function createPost(Post $post): bool
    {
        $dtb = BDConnexion::getDb(); // $this->db
        $query = $dtb->prepare('
            INSERT INTO post (id_utilisateur, titre, chapo, contenu)
            VALUES (:id_utilisateur, :titre, :chapo, :contenu);
        ');
        $queryExecution = $query->execute([
            ':id_utilisateur' => unserialize($_SESSION['utilisateur'])->getId(),
            ':titre' => $post->getTitre(),
            ':chapo' => $post->getChapo(),
            ':contenu' => $post->getContenu(),
        ]);
        if (!$queryExecution) {
            throw new RequeteException();
        }
        echo 'Valid';
        return true;
    }

    /**
     * @return array
     * @throws RequeteException
     * @throws SaisieIncorrecte
     */
    public function getAllValidPosts(): array
    {
        $posts = [];
        $dtb = BDConnexion::getDb();
        $query = $dtb->prepare('
            SELECT id_post, id_utilisateur, titre, chapo, contenu, date_publication, date_maj
            FROM post
            WHERE id_statut = :statut
            ORDER BY date_publication DESC;
        ');
        $query->execute([':statut' => self::ID_STATUT_VALID]);

        while ($post = $query->fetch()) {
            $temp_post = new Post();
            $temp_post->setId($post->id_post);
            $temp_post->setUtilisateur((new UtilisateurDTO())->getById($post->id_utilisateur));
            $temp_post->setTitre($post->titre);
            $temp_post->setChapo($post->chapo);
            $temp_post->setContenu($post->contenu);
            $temp_post->setDatePublication($post->date_publication);
            $temp_post->setDateMaj($post->date_maj);
            $temp_post->setCommentaires((new CommentaireDTO())->getPostCommentaires($temp_post));
            $posts[] = $temp_post;
        }

        return $posts;
    }

    /**
     * @param Post $post
     * @return bool
     * @throws RequeteException
     */
    public function deletePost(Post $post): bool
    {
        $dtb = BDConnexion::getDb();

        (new CommentaireDTO())->deletePostCommentaires($post);

        $query = $dtb->prepare('
            DELETE FROM post
            WHERE id_post = :id
        ');
        if (!$query->execute([':id' => $post->getId()])) {
            throw new RequeteException();
        }
        echo 'Valid';
        return true;
    }

    /**
     * @param Post $post
     * @return bool
     * @throws RequeteException
     */
    public function modifyPost(Post $post): bool
    {
        $dtb = BDConnexion::getDb();
        $query = $dtb->prepare('
            UPDATE post
            SET titre = :titre, chapo = :chapo, contenu = :contenu, id_utilisateur = :auteur, date_maj = NOW()
            WHERE id_post = :id
        ');
        $executedQuery = $query->execute([
            ':titre' => $post->getTitre(),
            ':chapo' => $post->getChapo(),
            ':contenu' => $post->getContenu(),
            ':auteur' => $post->getIdAuteur(),
            ':id' => $post->getId()
        ]);

        if (!$executedQuery) {
            throw new RequeteException();
        }

        echo 'Valid';
        return true;
    }


    /**
     * @param Post $post
     * @return bool
     * @throws RequeteException
     */
    public function existPost(Post $post): bool
    {
        $dtb = BDConnexion::getDB();
        $query = $dtb->prepare('
            SELECT id_post
            FROM post
            WHERE id_post = :id
        ');
        $query->execute([':id' => $post->getId()]);

        if (!$query->fetch()) {
            throw new RequeteException();
        }

        return true;
    }

    /**
     * @return array
     * @throws RequeteException
     * @throws SaisieIncorrecte
     */
    public function getAllInvalidPosts(): array
    {
        $posts = [];
        $dtb = BDConnexion::getDb();
        $query = $dtb->prepare('
            SELECT id_post, id_utilisateur, titre, chapo, contenu, date_publication, date_maj
            FROM post
            WHERE id_statut = :statut
            ORDER BY date_publication ASC;
        ');

        if (!$query->execute([':statut' => self::ID_STATUT_INVALID])) {
            throw new RequeteException();
        }

        while ($post = $query->fetch()) {
            $temp_post = new Post();
            $temp_post->setId($post->id_post);
            $temp_post->setUtilisateur((new UtilisateurDTO())->getById($post->id_utilisateur));
            $temp_post->setTitre($post->titre);
            $temp_post->setChapo($post->chapo);
            $temp_post->setContenu($post->contenu);
            $temp_post->setDatePublication($post->date_publication);
            $posts[] = $temp_post;
        }

        return $posts;
    }

    /**
     * @param Post $post
     * @return bool
     * @throws RequeteException
     */
    public function validPost(Post $post): bool
    {
        $dtb = BDConnexion::getDb();
        $query = $dtb->prepare('
            UPDATE post
            SET id_statut = :statut
            WHERE id_post = :id
        ');
        ;

        if (!$query->execute([':id' => $post->getId(), ':statut' => self::ID_STATUT_VALID])) {
            throw new RequeteException();
        }

        echo 'Valid';
        return true;
    }

    /**
     * @param int $id
     * @return Post
     * @throws RequeteException
     * @throws SaisieIncorrecte
     */
    public function getById(int $id): Post
    {
        $dtb = BDConnexion::getDb();
        $query = $dtb->prepare('
            SELECT id_post, id_utilisateur, titre, chapo, contenu, date_publication, date_maj
            FROM post
            WHERE id_post = :id;
        ');

        if (!$query->execute([':id' => $id])) {
            throw new RequeteException();
        }

        $post = null;
        if ($result = $query->fetch()) {
            $post = new Post();
            $post->setId($result->id_post);
            $post->setUtilisateur((new UtilisateurDTO())->getById($result->id_utilisateur));
            $post->setTitre($result->titre);
            $post->setChapo($result->chapo);
            $post->setContenu($result->contenu);
            $post->setDatePublication($result->date_publication);
            $post->setCommentaires((new CommentaireDTO())->getPostCommentaires($post));
        }

        return $post;
    }
}
