<?php

namespace Isoware\DTO;

use Isoware\BDConnexion;
use Isoware\Exception\ElementInexistantException;
use Isoware\Exception\RequeteException;
use Isoware\Model\Role;
use Isoware\Model\Utilisateur;

class RoleDTO
{
    /**
     * @return array
     * @throws RequeteException
     */
    public function getAll(): array
    {
        $dtb = BDConnexion::getDb();

        $query = $dtb->prepare('
            SELECT id_role, nom
            FROM role
            WHERE nom != \'admin_principal\'
        ');

        if (!$query->execute()) {
            throw new RequeteException();
        }

        $roles = [];
        while ($result = $query->fetch()) {
            $tempRole = new Role();
            $tempRole->setId($result->id_role);
            $tempRole->setNom($result->nom);
            $roles[] = $tempRole;
        }

        return $roles;
    }

    /**
     * @param int $idRole
     * @return Role
     * @throws ElementInexistantException
     * @throws RequeteException
     */
    public function getById(int $idRole): Role
    {
        $dtb = BDConnexion::getDb();

        $query = $dtb->prepare('
            SELECT nom
            FROM role
            WHERE id_role = :id_role;
        ');

        if (!$query->execute([':id_role' => $idRole])) {
            throw new RequeteException();
        }

        $role = new Role();

        if (!($result = $query->fetch())) {
            throw new ElementInexistantException();
        }

        $role->setId($idRole);
        $role->setNom($result->nom);

        return $role;
    }
}
