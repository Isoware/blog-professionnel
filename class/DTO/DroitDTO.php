<?php

namespace Isoware\DTO;

use Isoware\BDConnexion;
use Isoware\Exception\RequeteException;
use Isoware\Model\Droit;
use Isoware\Model\Role;

class DroitDTO
{
    /**
     * @param Role $role
     * @return array
     * @throws RequeteException
     */
    public function getDroitsByRole(Role $role): array
    {
        $dtb = BDConnexion::getDb();

        $query = $dtb->prepare('
            SELECT d.id_droit, d.nom
            FROM role_droits rd JOIN droit d
            WHERE rd.id_droit = d.id_droit
            AND rd.id_role = :id_role;
        ');
        if (!$query->execute([':id_role' => $role->getId()])) {
            throw new RequeteException();
        }

        $droits = [];
        while ($result = $query->fetch()) {
            $temp_droit = new Droit();
            $temp_droit->setId($result->id_droit);
            $temp_droit->setNom($result->nom);
            $droits[] = $temp_droit;
        }

        return $droits;
    }
}
