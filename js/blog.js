$(function () {
    // Create post Form
    $(
        "#createPostForm input,#createPostForm button,#createPostForm textarea"
    ).jqBootstrapValidation({
        preventSubmit: true,
        submitError($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM

            const titre = $("input#titrePost").val();
            const chapo = $("textarea#chapoPost").val();
            const contenu = $("textarea#contenuPost").val();
            const createPostButton = $("#createPostButton");
            createPostButton.prop("disabled", true); // Disable submit button until AJAX call is complete to prevent duplicate messages
            $.ajax({
                url: "index.php?a=createPost",
                type: "POST",
                data: {
                    titre,
                    chapo,
                    contenu,
                },
                cache: false,
                success(data) {
                    if (data === "Valid") {
                        $("#createPostForm").trigger("reset");
                        $("#createPostSuccess").html("<div class='alert alert-success'>");
                        $("#createPostSuccess > .alert-success").append(
                            $("<strong>").text(
                                "Votre article a été soumis pour validation !"
                            )
                        );
                        $("#createPostSuccess > .alert-success").append("</div>");
                    } else {
                        $("#createPostSuccess").html("<div class='alert alert-danger'>");
                        $("#createPostSuccess > .alert-danger").append(
                            $("<strong>").text(data)
                        );
                        $("#createPostSuccess > .alert-danger").append("</div>");
                    }
                },
                error() {
                    // Fail message
                    $("#createPost").html("<div class='alert alert-danger'>");
                    $("#createPostSuccess > .alert-danger")
                        .html(
                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;"
                        )
                        .append("</button>");
                    $("#createPostSuccess > .alert-danger").append(
                        $("<strong>").text(
                            "Désolé, il semble que le serveur ne répond pas, réessayez plus tard !"
                        )
                    );
                    $("#createPostSuccess > .alert-danger").append("</div>");
                },
                complete() {
                    setTimeout(function () {
                        createPostButton.prop("disabled", false); // Re-enable submit button when AJAX call is complete
                    }, 1000);
                },
            });
        },
        filter() {
            return $(this).is(":visible");
        },
    });

    $(".suppressPost").on("click", function () {
        if (confirm("Cet article et tous les commentaires associés seront supprimés.")) {
            let idPost = $(this).parents(".modalPost").data("id-post");
            $.ajax({
                url: "index.php?a=deletePost",
                type: "POST",
                data: {
                    id: idPost,
                },
                cache: false,
                success(data) {
                    if (data === "Valid") {
                        document.location.reload();
                    } else {
                        $("#divErrorPost" + idPost).html(data);
                    }
                }
            });
        }
    });

    $(".modifyPost").on("click", function () {
        let modal = $(this).parents(".modalPost");
        $(this).hide();
        $(".confirmModificationPost", modal).show();
        $(".postDetails", modal).hide();
        $(".modifyPostForm", modal).show();
    });

    $(".modifyPostForm input,.modifyPostForm textarea").jqBootstrapValidation({
        preventSubmit: true
    });

    $(".confirmModificationPost").on("click", function () {
        let modal = $(this).parents(".modalPost");
        let idPost = modal.data("id-post");

        $.ajax({
            url: "index.php?a=modifyPost",
            type: "POST",
            data: {
                id: idPost,
                titre: $(".modifyTitrePost", modal).val(),
                chapo: $(".modifyChapoPost", modal).val(),
                contenu: $(".modifyContenuPost", modal).val(),
                auteur: $(".modifyAuteurPost option:selected", modal).val(),
            },
            cache: false,
            success(data) {
                if (data === "Valid") {
                    document.location.reload();
                } else {
                    $("#divErrorPost" + idPost).html(data);
                }
            }
        });
    });

    $(".addComment textarea, .addComment button").jqBootstrapValidation({
        preventSubmit: true,
        submitSuccess(form, event) {
            event.preventDefault();
            let modal = $(event.target).parents(".modalPost");
            let idPost = modal.data("id-post");

            $.ajax({
                url: "index.php?a=addComment",
                type: "POST",
                data: {
                    idPost,
                    contenu: $(".contenuComment", modal).val(),
                },
                cache: false,
                success(data) {
                    $("#divSuccessAddComment" + idPost).html("");
                    $("#divErrorAddComment" + idPost).html("");

                    if (data === "Valid") {
                        $(".contenuComment", modal).val("");
                        $("#divSuccessAddComment" + idPost).html("Votre commentaire sera bientôt validé !");
                    } else {
                        $("#divErrorAddComment" + idPost).html(data);
                    }
                }
            });
        }
    });

    $(".suppressComment").on("click", function () {
        if (confirm("Ce commentaire sera supprimé.")) {
            let divCommentaire = $(this).parents(".commentaire");
            $.ajax({
                url: "index.php?a=deleteComment",
                type: "POST",
                data: {
                    idComment: divCommentaire.data("idcommentaire")
                },
                cache: false,
                success(data) {
                    if (data === "Valid") {
                        divCommentaire.remove();
                    }
                }
            });
        }
    });
});
