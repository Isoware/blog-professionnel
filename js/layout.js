$(function () {
    // Connection Form
    $(
        "#connectForm input,#connectForm button"
    ).jqBootstrapValidation({
        preventSubmit: true,
        submitError($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            const email = $("input#emailConnect").val();
            const password = $("input#password").val();
            const connectButton = $("#connectButton");
            connectButton.prop("disabled", true); // Disable submit button until AJAX call is complete to prevent duplicate messages
            $.ajax({
                url: "index.php?a=connect",
                type: "POST",
                data: {
                    email,
                    password
                },
                cache: false,
                success(data) {
                    if (data === "Valid") {
                        document.location.reload();
                    } else {
                        $("#connectError").html("<div class='alert alert-danger'>");
                        $("#connectError > .alert-danger").append(
                            $("<strong>").text(
                                "Les identifiants sont incorrects."
                            )
                        );
                        $("#connectError > .alert-danger").append("</div>");
                    }
                },
                error() {
                    // Fail message
                    $("#connectError").html("<div class='alert alert-danger'>");
                    $("#connectError > .alert-danger")
                        .html(
                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;"
                        )
                        .append("</button>");
                    $("#connectError > .alert-danger").append(
                        $("<strong>").text(
                            "Désolé, il semble que le serveur ne répond pas, réessayez plus tard !"
                        )
                    );
                    $("#connectError > .alert-danger").append("</div>");
                    //clear all fields
                    $("#connectForm").trigger("reset");
                },
                complete() {
                    setTimeout(function () {
                        connectButton.prop("disabled", false); // Re-enable submit button when AJAX call is complete
                    }, 1000);
                },
            });
        },
        filter() {
            return $(this).is(":visible");
        },
    });

    // Sign up Form
    $(
        "#signForm input,#signForm button"
    ).jqBootstrapValidation({
        preventSubmit: true,
        submitError($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess($form, event) {
            event.preventDefault();

            const nom = $("input#nameSign").val();
            const prenom = $("input#firstNameSign").val();
            const pseudo = $("input#pseudoSign").val();
            const email = $("input#emailSign").val();
            const password = $("input#passwordSign").val();
            const signButton = $("#signButton");
            signButton.prop("disabled", true); // Disable submit button until AJAX call is complete to prevent duplicate messages
            $.ajax({
                url: "index.php?a=sign",
                type: "POST",
                data: {
                    nom,
                    prenom,
                    pseudo,
                    email,
                    password,
                    recaptchaResponse: $("#recaptchaResponse").val()
                },
                cache: false,
                success(data) {
                    if (data === 'Valid') {
                        $("#signForm").trigger("reset");
                        $("#signError").html("<div class='alert alert-success'>");
                        $("#signError > .alert-success").append(
                            $("<strong>").text(
                                "Vous êtes inscrit ! Vous pouvez désormais vous connecter."
                            )
                        );
                        $("#signError > .alert-success").append("</div>");
                    } else {
                        $("#signError").html("<div class='alert alert-danger'>");
                        $("#signError > .alert-danger").append(
                            $("<strong>").text(
                                data
                            )
                        );
                        $("#signError > .alert-danger").append("</div>");
                    }
                },
                error() {
                    // Fail message
                    $("#signError").html("<div class='alert alert-danger'>");
                    $("#signError > .alert-danger")
                        .html(
                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;"
                        )
                        .append("</button>");
                    $("#signError > .alert-danger").append(
                        $("<strong>").text(
                            "Désolé, il semble que le serveur ne répond pas, réessayez plus tard !"
                        )
                    );
                    $("#signError > .alert-danger").append("</div>");
                },
                complete() {
                    setTimeout(function () {
                        signButton.prop("disabled", false); // Re-enable submit button when AJAX call is complete
                    }, 1000);
                },
            });
        },
        filter() {
            return $(this).is(":visible");
        },
    });

    $("#disconnectButton").on("click", function () {
        $.ajax({
            url: "index.php?a=disconnect",
            success() {
                location.href = "/home";
            },
        });
    });

    grecaptcha.ready(function () {
        grecaptcha.execute("6LdLhoIaAAAAANgpORt_9N_B3DA4y6WHU4fj3AjD", {action: "homepage"}).then(function (token) {
            document.getElementById("recaptchaResponse").value = token;
        });
    });
});
