$(function (events, handler) {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });

    let navbarCollapse = function () {
        if ($("#mainNav").offset().top > 100) {
            $("#sidebar").addClass("sidebar-shrink");
        } else {
            $("#sidebar").removeClass("sidebar-shrink");
        }
    };

    $(window).on('scroll', function () {
        navbarCollapse();
    });
});
