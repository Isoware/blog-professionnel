$(function (events, handler) {
    $(".suppressComment").on("click", function () {
        if (confirm("Ce commentaire sera supprimé.")) {
            let ligneTableau = $(this).parents("tr");
            $.ajax({
                url: "index.php?a=deleteComment",
                type: "POST",
                data: {
                    idComment: ligneTableau.data("idcomment")
                },
                cache: false,
                success(data) {
                    if (data === "Valid") {
                        ligneTableau.remove();
                    }
                }
            });
        }
    });

    $(".validComment").on("click", function () {
        if (confirm("Ce commentaire sera validé.")) {
            let ligneTableau = $(this).parents("tr");
            $.ajax({
                url: "index.php?a=validComment",
                type: "POST",
                data: {
                    idComment: ligneTableau.data("idcomment")
                },
                cache: false,
                success(data) {
                    if (data === "Valid") {
                        ligneTableau.remove();
                    }
                }
            });
        }
    });
});
