$(function (events, handler) {
    $(".suppressPost").on("click", function () {
        if (confirm("Cet article sera supprimé.")) {
            let ligneTableau = $(this).parents("tr");
            $.ajax({
                url: "index.php?a=suppressPost",
                type: "POST",
                data: {
                    idPost: ligneTableau.data("idpost")
                },
                cache: false,
                success(data) {
                    if (data === "Valid") {
                        ligneTableau.remove();
                    }
                }
            });
        }
    });

    $(".validPost").on("click", function () {
        if (confirm("Cet article sera validé.")) {
            let ligneTableau = $(this).parents("tr");
            $.ajax({
                url: "index.php?a=validPost",
                type: "POST",
                data: {
                    idPost: ligneTableau.data("idpost")
                },
                cache: false,
                success(data) {
                    if (data === "Valid") {
                        ligneTableau.remove();
                    }
                }
            });
        }
    });
});
