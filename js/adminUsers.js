$(function (events, handler) {
    let refreshFilters = function (event) {
        let button = $(event.target);

        if (button.data("type") === "tous") {
            if (!button.hasClass("active")) {
                $("#filtreUtilisateurs .btn.active").removeClass("active");
                button.toggleClass("active");
                $(".tableauUtilisateurLigne").show();
                return;
            }
        } else {
            $("#filtreUtilisateurs .btn.active[data-type='tous']").removeClass("active");
        }

        button.toggleClass("active");

        for (let i = 1; i <= 4; i++) {
            if ($("[data-idrole='" + i + "'].active", this).length === 1) {
                $("tr[data-idrole='" + i + "']").show();
            } else {
                $("tr[data-idrole='" + i + "']").hide();
            }
        }
    };

    $("#filtreUtilisateurs").on("click", refreshFilters);

    $(".selectRoleUtilisateur").on("change", function (e) {
        let ligneUtilisateur = $(this).parents("tr");
        let idRole = $(this).val();
        $.ajax({
            url: "index.php?a=modifyUserRole",
            type: "POST",
            data: {
                idUtilisateur: ligneUtilisateur.data("iduser"),
                idRole
            },
            cache: false,
            success(data) {
                ligneUtilisateur.attr("data-idrole", idRole);
                $("#filtreUtilisateurs").off("click")
                                                .on("click", refreshFilters);
            }
        });
    });

    $(".suppressUser").on("click", function () {
        if (confirm("Vraiment ?")) {
            let ligneTableau = $(this).parents("tr");
            $.ajax({
                url: "index.php?a=suppressUser",
                type: "POST",
                data: {
                    idUtilisateur: ligneTableau.data("iduser")
                },
                cache: false,
                success(data) {
                    if (data === "Valid") {
                        ligneTableau.remove();
                    }
                }
            });
        }
    });
});
