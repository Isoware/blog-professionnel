<?php

use Isoware\Router;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$_SESSION['root'] = realpath($_SERVER["DOCUMENT_ROOT"]) . '/';

require_once 'vendor/autoload.php';
require_once 'class/CustomAutoloader.php';
Isoware\CustomAutoloader::register();

$page = htmlentities($_GET['p'] ?? $_SESSION['current_page'] ?? 'home', ENT_QUOTES, 'UTF-8');
$action = htmlentities($_GET['a'] ?? null, ENT_QUOTES, 'UTF-8');

new Router($page, $action);
